package api.test;

import br.com.allsides.bdd.Feature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ScenarioTest {

    @Test
    void test() {
        String umObjeto = "umObjeto: ";
        Feature.name("Trocas de Usuários stocks", umObjeto::concat)
        
        .scenario("O usuário solicita uma venda antes de fechamento do pregão", Integer.class)
        .given("Dado que tenho 100 ações da MSFT", () -> 1234)
        .when("Quando eu pedir para vender 20 ações da MSFT", x -> x.toString())
        .then("Então eu deveria ter 80 ações da MSFT", x -> false)
        
        .scenario("O usuário solicita uma venda antes de fechamento do pregão")
        .given("Dado que tenho 100 ações da MSFT", () -> "1234")
        .when("Quando eu pedir para vender 20 ações da MSFT", x -> x.toString())
        .then("Então eu deveria ter 80 ações da MSFT", "umObjeto: 1234", Assertions::assertEquals);
        
        
        
        
        
//        Scenario.name("Elevar um número ao quadrado")
//        .given("4", () -> 4)
//        .when("Elevado ao quadrado", x -> x * x)
//        .then("é 16", x -> x == 16);
    }
    
//    int quatro() {
//        return 4;
//    }
//    
//    int elevado_ao_quadrado(int umNumero) {
//        return umNumero * umNumero;
//    }
//    
//    boolean é_16(int x) {
//        return x == 16;
//    }

}
