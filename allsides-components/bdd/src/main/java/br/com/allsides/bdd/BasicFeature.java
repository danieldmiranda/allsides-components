package br.com.allsides.bdd;

import br.com.allsides.functional.Mapper;

class BasicFeature<T, R> implements Feature<T, R> {
    
    private String name;
    private Mapper<T, R> feature;

    BasicFeature(String name, Mapper<T, R> feature) { 
        this.name = name;
        this.feature = feature;
    }

    @Override
    public Scenario<T, T, R> scenario(String name) {
        return BasicScenario.scenario(this, name);
    }

    @Override
    public <A> Scenario<A, T, R> scenario(String name, Class<A> contextClass) {
        return BasicScenario.scenario(this, name, contextClass);
    }

    R apply(T t) {
        System.out.println(name);
        return feature.apply(t);
    }

}
