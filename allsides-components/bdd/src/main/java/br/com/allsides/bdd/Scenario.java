package br.com.allsides.bdd;

import java.util.function.BiConsumer;

import br.com.allsides.functional.Mapper;
import br.com.allsides.functional.Provider;
import br.com.allsides.functional.Specification;

public interface Scenario<T, U, R> {
    
    Scenario<T, U, R> given(String description, Provider<T> given);
    
    Scenario<T, U, R> when(String description, Mapper<T, U> when);
    
    Feature<U, R> then(String description, Specification<R> then);
    
    Feature<U, R> then(String description, R expected, BiConsumer<R, R> assertion);
    
}
