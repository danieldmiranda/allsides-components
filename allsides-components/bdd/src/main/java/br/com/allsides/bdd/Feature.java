package br.com.allsides.bdd;

import br.com.allsides.functional.Mapper;

public interface Feature<T, R> {
    
    static <B, C> Feature<B, C> name(String name, Mapper<B, C> feature) { 
        return new BasicFeature<B, C>(name, feature);
    }
        
    Scenario<T, T, R> scenario(String name);
    
    <A> Scenario<A, T, R> scenario(String name, Class<A> contextClass);
    
}
