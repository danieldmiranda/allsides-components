package br.com.allsides.bdd;

import br.com.allsides.functional.Mapper;
import br.com.allsides.functional.Provider;

public class ScenarioResult<T, U, R> {
    
    private final String name;
    private final Provider<T> given;
    private final Mapper<T, U> when;
    private final String givenDescription;
    private final String whenDescription;
    
    public ScenarioResult(String name) {
        this(name, "", null, "", null);
    }
    
    public ScenarioResult(String name, String givenDescription, Provider<T> given, String whenDescription, Mapper<T, U> when) {
        this.name = name;
        this.givenDescription = givenDescription;
        this.given = given;
        this.whenDescription = whenDescription;
        this.when = when;
    }

    public String getName() {
        return name;
    }

    public Provider<T> getGiven() {
        return given;
    }

    public Mapper<T, U> getWhen() {
        return when;
    }

    public String getGivenDescription() {
        return givenDescription;
    }

    public String getWhenDescription() {
        return whenDescription;
    }
    
    public ScenarioResult<T, U, R> given(String description, Provider<T> given) {
        return new ScenarioResult<>(name, description, given, "", null);
    }

    public ScenarioResult<T, U, R> when(String description, Mapper<T, U> when) {
        return new ScenarioResult<>(name, givenDescription, given, description, when);
    }
    
}
