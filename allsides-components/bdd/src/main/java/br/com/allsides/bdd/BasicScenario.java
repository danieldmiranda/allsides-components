package br.com.allsides.bdd;

import java.util.function.BiConsumer;

import br.com.allsides.functional.Mapper;
import br.com.allsides.functional.Provider;
import br.com.allsides.functional.Specification;

final class BasicScenario<T, U, R> implements Scenario<T, U, R> {

    private final ScenarioResult<T, U, R> result;
    private final BasicFeature<U, R> feature;

    private BasicScenario(BasicFeature<U, R> feature, String name) {
        this(feature, new ScenarioResult<>(name));
    }
    
    private BasicScenario(BasicFeature<U, R> feature, ScenarioResult<T, U, R> result) {
        this.result = result;
        this.feature = feature;
    }
    
    public static <A, B, C> Scenario<A, B, C> scenario(BasicFeature<B, C> feature, String name) {
        return new BasicScenario<>(feature, name);
    }
    
    public static <A, B, C> Scenario<A, B, C> scenario(BasicFeature<B, C> feature, String name, Class<A> contextClass) {
        return scenario(feature, name);
    }
    
    @Override
    public Scenario<T, U, R> given(String description, Provider<T> given) {
        return new BasicScenario<>(feature, result.given(description, given));
    }

    @Override
    public Scenario<T, U, R> when(String description, Mapper<T, U> when) {
        return new BasicScenario<>(feature, result.when(description, when));
    }

    @Override
    public Feature<U, R> then(String description, Specification<R> then) {
        boolean test = then.test(feature.apply(result.getWhen().apply(result.getGiven().get())));
        System.out.println(result.getName());
        System.out.println(result.getGivenDescription());
        System.out.println(result.getWhenDescription());
        System.out.println(description);
        System.out.println(test);
        return feature;
    }

    @Override
    public Feature<U, R> then(String description, R expected, BiConsumer<R, R> assertion) {
        R resultado = feature.apply(result.getWhen().apply(result.getGiven().get()));
        assertion.accept(expected, resultado);
        return feature;
    }   

}
