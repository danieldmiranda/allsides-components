package br.com.allsides.provider.object;

import br.com.allsides.functional.Provider;

public interface ObjectProvider {

    <T> T get(Class<T> clazz);

    <T> Provider<T> getProvider(Class<T> clazz);

}
