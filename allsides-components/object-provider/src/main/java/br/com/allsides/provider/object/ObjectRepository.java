package br.com.allsides.provider.object;

import java.util.HashMap;
import java.util.Map;

import br.com.allsides.functional.Mapper;
import br.com.allsides.functional.Provider;

public class ObjectRepository implements ObjectProvider, ObjectRegister {

    public static final ObjectRepository INSTANCIA = new ObjectRepository();
    private final Map<Class<?>, Provider<?>> objs = new HashMap<>();
    
    public static ObjectRegister register() {
        return INSTANCIA;
    }
    
    public static ObjectProvider provider() {
        return INSTANCIA;
    }

    @Override
    public <T> void addSingleton(Class<T> clazz, Provider<T> supplier) {
        objs.put(clazz, new InstanceCache<>(supplier));
    }

    @Override
    public <T> void addSingleton(Class<T> clazz, Mapper<ObjectProvider, T> mapper) {
        addSingleton(clazz, () -> mapper.map(this));
    }

    @Override
    public <T> void addFactory(Class<T> clazz, Provider<T> supplier) {
        objs.put(clazz, supplier);
    }

    @Override
    public <T> void addFactory(Class<T> clazz, Mapper<ObjectProvider, T> mapper) {
        addSingleton(clazz, () -> mapper.map(this));
    }

    @Override
    public <T> T get(Class<T> clazz) {
        return (T) objs.get(clazz).get();
    }

    @Override
    public <T> Provider<T> getProvider(Class<T> clazz) {
        return () -> get(clazz);
    }

    private static class InstanceCache<T> implements Provider<T> {
        
        private T instance;
        private final Provider<T> supplier;

        public InstanceCache(Provider<T> instance) {
            this.supplier = instance;
        }

        @Override
        public T provide() throws Exception {
            if (instance == null) {
                instance = supplier.provide();
            }
            return instance;
        }

    }

}
