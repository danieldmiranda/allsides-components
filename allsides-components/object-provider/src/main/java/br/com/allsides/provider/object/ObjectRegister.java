package br.com.allsides.provider.object;

import br.com.allsides.functional.Mapper;
import br.com.allsides.functional.Provider;

public interface ObjectRegister {

    <T> void addSingleton(Class<T> clazz, Provider<T> supplier);

    <T> void addSingleton(Class<T> clazz, Mapper<ObjectProvider, T> mapper);

    <T> void addFactory(Class<T> clazz, Provider<T> supplier);

    <T> void addFactory(Class<T> clazz, Mapper<ObjectProvider, T> mapper);

}
