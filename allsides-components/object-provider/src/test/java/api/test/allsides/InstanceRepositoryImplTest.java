package api.test.allsides;

import java.util.Date;

import br.com.allsides.provider.object.ObjectRegister;
import br.com.allsides.provider.object.ObjectRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InstanceRepositoryImplTest {

    @Test
    void addFactory() {
        ObjectRepository.register().addFactory(String.class, () -> "anything");
        String result = ObjectRepository.provider().get(String.class);
        assertEquals("anything", result);
    }
    
    @Test
    void getProvider() {
        ObjectRepository.register().addFactory(String.class, () -> "anything");
        String result = ObjectRepository.provider().getProvider(String.class).get();
        assertEquals("anything", result);
    }
    
    @Test
    void addSingleton() {
        ObjectRepository.register().addSingleton(Date.class, () -> new Date());
        Date first = ObjectRepository.provider().get(Date.class);
        Date secund = ObjectRepository.provider().get(Date.class);
        assertEquals(first, secund);
    }
    
    @Test
    void addFactoryWithDependencies() {
        ObjectRegister register = ObjectRepository.register();
        register.addFactory(Integer.class, () -> 42);
        register.addFactory(String.class, provider -> "anything" + provider.get(Integer.class));
        String result = ObjectRepository.provider().get(String.class);
        assertEquals("anything42", result);
    }
    
    @Test
    void addSingletonWithDependencies() throws InterruptedException {
        ObjectRegister register = ObjectRepository.register();
        register.addSingleton(Long.class, () -> new Date().getTime());
        register.addSingleton(String.class, provider -> "anything" + provider.get(Long.class));
        String first = ObjectRepository.provider().get(String.class);
        Thread.sleep(2);
        String secund = ObjectRepository.provider().get(String.class);
        assertEquals(first, secund);
    }

}
