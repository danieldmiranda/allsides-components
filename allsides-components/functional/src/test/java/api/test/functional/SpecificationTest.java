package api.test.functional;

import br.com.allsides.functional.Specification;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

class SpecificationTest {

    @Test
    void delegateTest() throws Exception {
        Specification<String> spec = text -> text.isEmpty();
        assertFalse(spec.test("anything"));
    }

}
