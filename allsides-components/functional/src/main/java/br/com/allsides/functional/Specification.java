package br.com.allsides.functional;

import java.util.function.Predicate;

public interface Specification<T> extends Predicate<T>, Mapper<T, Boolean> {
    
    boolean check(T t) throws Exception;
    
    @Override
    default Boolean map(T t) throws Exception {
        return check(t);
    }
    
    default boolean test(T t) {
        return apply(t);
    }

}
